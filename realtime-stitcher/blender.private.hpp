#pragma once

#include <cstdint>

#include <opencv2/stitching.hpp>

namespace thi_sensor_fusion {

class SelectiveBlender : public cv::detail::Blender {
public:
  SelectiveBlender(uint8_t index);
  ~SelectiveBlender() = default;

  virtual void feed(cv::InputArray img, cv::InputArray mask, cv::Point tl) override;
  void setIndexOfInterest(uint8_t index);

protected:
  uint8_t index_of_interest;
};

}
