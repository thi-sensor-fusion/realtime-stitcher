#include <thi-sensor-fusion/realtime-stitcher/blender.private.hpp>


namespace thi_sensor_fusion {

SelectiveBlender::SelectiveBlender(const uint8_t index) {
  setIndexOfInterest(index);
}

void SelectiveBlender::feed(cv::InputArray _img, cv::InputArray _mask, cv::Point tl) {
  cv::Mat img = _img.getMat();
  cv::Mat mask = _mask.getMat();
  cv::Mat dst = dst_.getMat(cv::ACCESS_RW);
  cv::Mat dst_mask = dst_mask_.getMat(cv::ACCESS_RW);

  CV_Assert(img.type() == CV_16SC3);
  CV_Assert(mask.type() == CV_8U);
  int dx = tl.x - dst_roi_.x;
  int dy = tl.y - dst_roi_.y;

  for (int y = 0; y < img.rows; ++y) {
    const cv::Point3_<short> *src_row = img.ptr<cv::Point3_<short> >(y);
    cv::Point3_<short> *dst_row = dst.ptr<cv::Point3_<short> >(dy + y);
    const uchar *mask_row = mask.ptr<uchar>(y);
    uchar *dst_mask_row = dst_mask.ptr<uchar>(dy + y);

    for (int x = 0; x < img.cols; ++x) {
      if (mask_row[x] && (index_of_interest == 0)) {
        dst_row[dx + x] = src_row[x];
      }

      dst_mask_row[dx + x] |= mask_row[x];
    }
  }

  --index_of_interest;
}

void SelectiveBlender::setIndexOfInterest(const uint8_t index) {
  index_of_interest = index;
}

}
