#include <iostream>
#include <vector>
#include <chrono>
#include <cstdint>
#include <regex>
#include <deque>
#include <memory>
#include <thread>

#include <opencv2/stitching.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/cudacodec.hpp>
#include <opencv2/videoio.hpp>

#include <thi-sensor-fusion/realtime-stitcher/stitcher.hpp>


const std::string generateCapturePipeline(uint8_t index, uint16_t width, uint16_t height, uint16_t framerate) {
  return "nvarguscamerasrc sensor-id=" + std::to_string(index) + " ! video/x-raw(memory:NVMM), width=(int)" + std::to_string(width) + ", height=(int)" + std::to_string(height) + ", framerate=(fraction)" + std::to_string(framerate) + "/1 ! nvvidconv flip-method=0 ! video/x-raw, width=(int)" + std::to_string(width) + ", height=(int)" + std::to_string(height) + ", format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink";
}

const std::string generateEncodePipeline(std::string host, uint16_t port, uint16_t width, uint16_t height) {
  return "appsrc ! videoconvert ! video/x-raw, width=(int)" + std::to_string(width) + ", height=(int)" + std::to_string(height) + " ! nvvidconv ! nvv4l2h264enc idrinterval=1 control-rate=0 peak-bitrate=40000000 ! rtph264pay ! udpsink host=" + host + " port=" + std::to_string(port);
}

void printUsage() {
  std::cout << "Usage: switch-test ncameras [index0, ... indexn]" << std::endl;
  exit(1);
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printUsage();
  }

  const uint16_t width = 2592;
  const uint16_t height = 1944;
  const uint16_t framerate = 30;

  const uint16_t drift_width = 0;
  const uint16_t drift_height = 0;

  const uint16_t number_modules_width = 2;
  const uint16_t number_modules_height = 1;
  const uint16_t number_modules = number_modules_width * number_modules_height;

  const uint16_t effective_input_width = (width / number_modules_width) - drift_width;
  const uint16_t effective_input_height = (height / number_modules_height) - drift_height;

  const uint16_t number_cameras = std::stoi(argv[1]);

  const float resolution_factor = 1;
  const float registration_scaling = 1;

  const uint16_t output_resolution_x = 1024;
  const uint16_t output_resolution_y = 1024;

  const thi_sensor_fusion::RealtimeStitcher::Settings settings = {
    .threads_cpu = 6,

    .threads_gpu = {
      .x = 8,
      .y = 8,
    },

    .output_resolution = {
      .x = output_resolution_x,
      .y = output_resolution_y,
    },

    .registration_scaling = {
      .x = registration_scaling * (float)width / (float)effective_input_width,
      .y = registration_scaling * (float)height / (float)effective_input_height,
    },
    .blending_priority_method = thi_sensor_fusion::RealtimeStitcher::Settings::PriorityMethod::upper_center,
    .blending_sharpness = 0.0004 * resolution_factor / std::sqrt(settings.registration_scaling.x * settings.registration_scaling.y),
    .visible_pitch_angle = 1.13 * std::numbers::pi,

    .enable_console_output = true,
  };

  /*
  std::vector<cv::Mat> frames(number_cameras);

  for (uint16_t i = 0; cv::Mat &frame : frames) {
    const std::string format(argv[2]);
    const std::string file = std::regex_replace(format, std::regex("%i"), std::to_string(i));

    frame = cv::imread(file, cv::IMREAD_COLOR);

    ++i;
  }
  */

  /*
  std::vector<cv::Mat> frames(number_cameras / 2);

  for (uint16_t i = 0; cv::Mat &frame : frames) {
    const std::string format(argv[2]);
    const std::string file = std::regex_replace(format, std::regex("%i"), std::to_string(i));

    frame = cv::imread(file, cv::IMREAD_COLOR);

    ++i;
  }
  */

  const double focal = 550;

  const std::vector<thi_sensor_fusion::RealtimeStitcher::CameraParameters> parameters = {
    {
      .focal = focal / resolution_factor,
      .aspect = 1,
      .dimensions = {
        .x = effective_input_width,
        .y = effective_input_height,
      },
      .rotation = {
        .heading = 0,
        .pitch = -46,
        .roll = 180,
      },
    },
    {
      .focal = focal / resolution_factor,
      .aspect = 1,
      .dimensions = {
        .x = effective_input_width,
        .y = effective_input_height,
      },
      .rotation = {
        .heading = -90,
        .pitch = -46,
        .roll = 180,
      },
    },
        {
      .focal = focal / resolution_factor,
      .aspect = 1,
      .dimensions = {
        .x = effective_input_width,
        .y = effective_input_height,
      },
      .rotation = {
        .heading = 180,
        .pitch = -46,
        .roll = 180,
      },
    },
    {
      .focal = focal / resolution_factor,
      .aspect = 1,
      .dimensions = {
        .x = effective_input_width,
        .y = effective_input_height,
      },
      .rotation = {
        .heading = 90,
        .pitch = -46,
        .roll = 180,
      },
    },
  };

  thi_sensor_fusion::RealtimeStitcher stitcher(settings, parameters, "buffer.dat");

  /*
  const std::vector<thi_sensor_fusion::RealtimeStitcher::CameraParameters> estimated_parameters = stitcher.estimateCameraParameters(frames);

  stitcher.setCameraParameters(estimated_parameters);
  stitcher.calculateTransformations();
  */

  std::vector<cv::VideoCapture> captures(number_cameras / number_modules);

  cv::Mat test_frame;

  for (uint8_t i = 1; cv::VideoCapture &capture : captures) {
    const std::string pipeline = generateCapturePipeline(i, width, height, framerate);
    std::cout << pipeline << std::endl;

    capture = cv::VideoCapture(pipeline);

    cv::Mat frame;
    capture >> frame;
    test_frame = frame;

    cv::InputArray image(frame);
    cv::imwrite("input" + std::to_string(i) + ".png", image);

    --i;
  }

  float heading = 0;
  float pitch = 0;
  float zoom = 1;

  const std::string encode_pipeline = generateEncodePipeline("10.0.0.10", 1234, output_resolution_x, output_resolution_y);
  std::cout << encode_pipeline << std::endl;

  cv::VideoWriter writer = cv::VideoWriter(encode_pipeline, cv::CAP_GSTREAMER, 0, (double)framerate, cv::Size(output_resolution_x, output_resolution_y), true);

  std::chrono::time_point<std::chrono::steady_clock> timestamp_last_frame = std::chrono::steady_clock::now();
  std::chrono::microseconds average_frame_time;

  const uint16_t count_threads = 1;

  std::mutex capture_mutex;
  std::mutex encode_mutex;
  std::mutex stats_mutex;
  std::atomic<uint16_t> frame_begin_index = 0;
  std::atomic<uint16_t> frame_end_index = 0;
  std::vector<std::thread> threads(count_threads);

  auto compose_loop = [&](uint16_t index) {
    struct Timestamps {
      std::chrono::time_point<std::chrono::steady_clock> begin;
      std::chrono::time_point<std::chrono::steady_clock> capture;
      std::chrono::time_point<std::chrono::steady_clock> upload;
      std::chrono::time_point<std::chrono::steady_clock> compose;
      std::chrono::time_point<std::chrono::steady_clock> download;
      std::chrono::time_point<std::chrono::steady_clock> annotate;
      std::chrono::time_point<std::chrono::steady_clock> encode;
      std::chrono::time_point<std::chrono::steady_clock> end;
    };

    Timestamps current_timestamps;
    Timestamps previous_timestamps;

    cv::cuda::Stream stream;

    bool first_iteration = true;

    while (true) {
      while (!frame_begin_index.compare_exchange_weak(index, (index + 1) % count_threads)) {
        frame_begin_index.wait((index + count_threads - 1) % count_threads);
      }

      current_timestamps.begin = std::chrono::steady_clock::now();
      current_timestamps.capture = current_timestamps.begin;
      std::vector<cv::Mat> frames(number_cameras / number_modules);

      {
        const std::lock_guard guard(capture_mutex);
        frame_begin_index.notify_all();

        for (uint16_t i = 0; cv::Mat &frame : frames) {
          captures[i] >> frame;

          if (first_iteration) {
            cv::imwrite("input_" + std::to_string(index) + "_" + std::to_string(i) + ".png", frame);
          }

          ++i;
        }

        first_iteration = false;
      }

      current_timestamps.upload = std::chrono::steady_clock::now();
      std::vector<cv::cuda::GpuMat> gpu_frames(number_cameras);

      for (uint16_t i = 0; cv::cuda::GpuMat &gpu_frame : gpu_frames) {
        const uint16_t offset_width = (i % number_modules_width) * effective_input_width;
        const uint16_t offset_height = ((i % number_modules_height) / number_modules_width) * effective_input_height;

        const cv::Rect roi(offset_width, offset_height, effective_input_width, effective_input_height);

        const cv::Mat local_frame(frames[i / number_modules], roi);
        gpu_frame = cv::cuda::GpuMat(local_frame);

        ++i;
      }

      current_timestamps.compose = std::chrono::steady_clock::now();
      cv::cuda::GpuMat output;
      stitcher.compose(output, gpu_frames, heading, pitch, zoom, stream);

      current_timestamps.download = std::chrono::steady_clock::now();
      cv::Mat panorama;
      output.download(panorama, stream);

      //heading += 0.05;

      current_timestamps.annotate = std::chrono::steady_clock::now();
      std::vector<std::string> annotation;
      const float frames_per_second = ((float)std::micro::den / (float)std::micro::num) / (float)average_frame_time.count();
      annotation.push_back("fps: " + std::to_string((uint32_t)frames_per_second));
      annotation.push_back("total: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.end - previous_timestamps.begin).count()) + "ms");
      annotation.push_back("capture: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.upload - previous_timestamps.capture).count()) + "ms");
      annotation.push_back("upload: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.compose - previous_timestamps.upload).count()) + "ms");
      annotation.push_back("compose: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.download - previous_timestamps.compose).count()) + "ms");
      annotation.push_back("download: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.annotate - previous_timestamps.download).count()) + "ms");
      annotation.push_back("annotate: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.encode - previous_timestamps.annotate).count()) + "ms");
      annotation.push_back("encode: " + std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(previous_timestamps.end - previous_timestamps.encode).count()) + "ms");
      
      const uint32_t padding = 5;
      uint32_t y_offset = 0;
      for (const std::string &line : annotation) {
        const uint32_t font_face = cv::FONT_HERSHEY_DUPLEX;
        const double font_scale = 1;
        const uint32_t font_thickness = 2;
        int baseline;

        const cv::Size size = cv::getTextSize(line, font_face, font_scale, font_thickness, &baseline);
        y_offset += size.height + padding;

        cv::putText(panorama, line, cv::Point(padding, y_offset), font_face, font_scale, CV_RGB(0xff, 0xff, 0xff), font_thickness, cv::LINE_AA);
      }
      
      current_timestamps.encode = std::chrono::steady_clock::now();
      while (!frame_end_index.compare_exchange_weak(index, (index + 1) % count_threads)) {
        frame_end_index.wait((index + count_threads - 1) % count_threads);
      }

      {
        const std::lock_guard guard(encode_mutex);
        frame_end_index.notify_all();

        writer << panorama;
      }
      
      current_timestamps.end = std::chrono::steady_clock::now();

      {
        const std::lock_guard guard(stats_mutex);

        average_frame_time = std::chrono::microseconds((average_frame_time + std::chrono::duration_cast<std::chrono::microseconds>(current_timestamps.end - timestamp_last_frame)).count() / 2);
        timestamp_last_frame = current_timestamps.end;
      }

      previous_timestamps = current_timestamps;
    }
  };

  for (uint16_t i = 0; std::thread &thread : threads) {
    thread = std::thread(compose_loop, i);
    ++i;
  }

  for (std::thread &thread : threads) {
    thread.join();
  }

  return 0;
}
