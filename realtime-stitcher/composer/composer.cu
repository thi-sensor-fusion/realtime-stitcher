#include <thi-sensor-fusion/realtime-stitcher/composer/composer.private.cuh>

#include <cstdint>
#include <cstdio>

#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/device_vector.h>


namespace thi_sensor_fusion {

__global__ void composeKernel(const cv::cudev::PtrStep<thi_sensor_fusion::LookupElement> lookup, const cv::cudev::PtrStep<uint8_t> *frames, cv::cudev::PtrStep<uint8_t> output, uint8_t sources, uint32_t limit_x, uint32_t limit_y) {
  static const uint8_t channel_count = 3;

  // Calculate the base index for this call.
  const uint32_t base_index_x = blockIdx.x * blockDim.x + threadIdx.x;
  const uint32_t base_index_y = blockIdx.y * blockDim.y + threadIdx.y;

  // Convert the base index to the output index.
  const uint32_t base_output_index_x = base_index_x;
  const uint32_t base_output_index_y = base_index_y * channel_count;

  // Convert the base index to the lookup index.
  const uint32_t base_lookup_index_x = base_index_x;
  const uint32_t base_lookup_index_y = base_index_y * sources;
    
  // Return if the index is out of bounds.
  if (base_index_x >= limit_x || base_index_y >= limit_y) {
    return;
  }

  // Iterate through all sources.
  for (uint8_t i = 0; i < sources; ++i) {
    const LookupElement element = lookup(base_lookup_index_x, base_lookup_index_y + i);

    // Iterate through all channels.
    for (uint8_t j = 0; j < channel_count; ++j) {
      // Add the weighted input pixel of the current source to the output frame for each channel.
      output(base_output_index_x, base_output_index_y + j) += element.weight * frames[i](element.position.x, (element.position.y * channel_count) + j);
    }
  }
}

void callComposeKernel(const LookupBuffer lookup, const cv::cuda::GpuMat *frames, cv::OutputArray output, const uint16_t threads_x, const uint16_t threads_y, cv::cuda::Stream &stream) {
  const uint8_t sources = lookup.getDimensionIndex();
  const uint64_t limit = lookup.getDimension();

  // Get the lookup buffer.
  const cv::cudev::PtrStep<LookupElement> gpu_lookup = lookup.getMat();

  thrust::device_vector<cv::cudev::PtrStep<uint8_t>> gpu_frames_vector(sources);
  
  // Get the input frames.
  for (uint8_t i = 0; i < sources; ++i) {
    gpu_frames_vector[i] = frames[i];
  }

  const cv::cudev::PtrStep<uint8_t> *gpu_frames = thrust::raw_pointer_cast(gpu_frames_vector.data());

  // Create the ouptut frame of the correct dimensions.
  output.create(lookup.getDimensionX(), lookup.getDimensionY(), CV_8UC3);
  cv::cuda::GpuMat gpu_output = output.getGpuMat();
  gpu_output.setTo(0);

  // CUDA thread and block setup.
  dim3 threads(threads_x, threads_y);
  dim3 blocks(std::ceil(lookup.getDimensionX() / static_cast<double>(threads.x)), std::ceil(lookup.getDimensionY() / static_cast<double>(threads.y)));

  // Call the CUDA kernel.
  cudaStream_t native_stream = reinterpret_cast<cudaStream_t>(stream.cudaPtr());
  composeKernel<<<blocks, threads, 0, native_stream>>>(gpu_lookup, gpu_frames, gpu_output, sources, lookup.getDimensionX(), lookup.getDimensionY());

  cudaDeviceSynchronize();
}

}
