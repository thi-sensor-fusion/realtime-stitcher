#include <thi-sensor-fusion/realtime-stitcher/lookup_buffer.private.hpp>

#include <opencv2/core/mat.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/cudev/ptr2d/gpumat.hpp>


namespace thi_sensor_fusion {

void callComposeKernel(const LookupBuffer lookup, const cv::cuda::GpuMat *frames, cv::OutputArray output, uint16_t threads_x, uint16_t threads_y, cv::cuda::Stream &stream);

}
