#pragma once

#include <cstdint>
#include <vector>
#include <limits>
#include <string>
#include <numbers>

#include <opencv2/opencv.hpp>

namespace thi_sensor_fusion {

class LookupBuffer;


class RealtimeStitcher {
public:
  struct Settings {
    uint16_t threads_cpu = 1;

    struct GpuThreads {
      uint16_t x = 1;
      uint16_t y = 1;
    } threads_gpu;

    struct Dimensions {
      uint32_t x = 1028;
      uint32_t y = 1028;
    } output_resolution;

    double compositing_resolution = std::numeric_limits<double>::infinity();
    double registration_resolution = std::numeric_limits<double>::infinity();

    struct Scaling {
      float x = 2;
      float y = 2;
    } registration_scaling;
    
    enum class PriorityMethod {
      upper_center,
      lower_center,
    } blending_priority_method = PriorityMethod::upper_center;
    double blending_sharpness = 0.02;

    double visible_pitch_angle = std::numbers::pi;

    bool enable_console_output = false;

    bool enable_opencl = true;
    bool enable_cuda = true;
  };

  struct CameraParameters {
    double focal;
    double aspect;
  
    struct Dimensions {
      uint32_t x;
      uint32_t y;
    } dimensions;

    struct Rotation {
      float heading;
      float pitch;
      float roll; 
    } rotation;
  };

  /**
   * @brief Create a @c RealtimeStitcher instance.
   * 
   * @param[in] settings General stitcher settings to be set.
   * @param[in] parameters Camera parameters to be set.
   */
  RealtimeStitcher(const Settings &settings, const std::vector<CameraParameters> &parameters);

  /**
   * @brief Create a @c RealtimeStitcher instance.
   * @note Files may not be compatible between different versions and CPU architectures.
   * 
   * @param[in] settings General stitcher settings to be set.
   * @param[in] parameters Camera parameters to be set.
   * @param[in] path Path to the input lookup data file.
   */
  RealtimeStitcher(const Settings &settings, const std::vector<CameraParameters> &parameters, const std::string &path);

  /**
   * @brief Set the stitcher settings.
   * 
   * @param[in] settings General stitcher settings to be set.
   */
  inline void setSettings(const Settings &settings) {
    this->settings = settings;
  }

  /**
   * @brief Get the stitcher settings.
   * 
   * @return const Settings& stitcher settings.
   */
  inline const Settings &getSettings() const {
    return settings;
  }

  /**
   * @brief Set the camera parameters.
   * 
   * @param[in] parameters Camera parameters to be set.
   */
  inline void setCameraParameters(const std::vector<CameraParameters> &parameters) {
    this->parameters = parameters;
  }

  /**
   * @brief Get the camera parameters.
   * 
   * @return const std::vector<CameraParameters>& camera parameters.
   */
  inline const std::vector<CameraParameters> &getCameraParameters() const {
    return parameters;
  }


  /**
   * @brief Estimate the camera parameters.
   * @details The estimation might not include all frames and should therefore only be used for debugging purposes.
   * 
   * @param[in] frames Input frames to be used for the estimation.
   */
  std::vector<CameraParameters> estimateCameraParameters(const std::vector<cv::Mat> &frames) const;

  /**
   * @brief Calculate Transformations.
   * @details This should be done after settings or camera parameters have changed.
   */
  void calculateTransformations();

  /**
   * @brief Store the lookup data into a file.
   * @note Files may not be compatible between different versions and CPU architectures.
   * 
   * @param[in] path Path to the output file.
   */
  void storeLookupData(const std::string &path) const;

  /**
   * @brief Load the lookup data from a file.
   * @note Files may not be compatible between different versions and CPU architectures.
   * 
   * @param[in] path Path to the input file.
   */
  void loadLookupData(const std::string &path);

  /**
   * @brief Compose a panorama from input frames.
   * @details This is very fast as it only uses lookup data.
   * 
   * @param[out] output Ouptut panorama matrix. Must be of type @c cv::cuda::GpuMat> .
   * @param[in] frames Input frames to be used for stitching. Must be of type @c std::vector<cv::cuda::GpuMat>> .
   * @param[in] heading Heading angle. (rad)
   * @param[in] pitch Pitch angle. (rad)
   * @param[in] zoom Zoom factor.
   */
  void compose(cv::OutputArray output, cv::InputArrayOfArrays frames, float heading = 0, float pitch = 0, float zoom = 1, cv::cuda::Stream &stream = cv::cuda::Stream::Null()) const;

private:
  Settings settings;
  std::vector<CameraParameters> parameters;

  LookupBuffer &lookup_buffer;

  static constexpr uint16_t version = GIT_VERSION_MAJOR;
};

}
