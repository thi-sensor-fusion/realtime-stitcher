#include <thi-sensor-fusion/realtime-stitcher/stitcher.hpp>
#include <thi-sensor-fusion/realtime-stitcher/blender.private.hpp>
#include <thi-sensor-fusion/realtime-stitcher/lookup_buffer.private.hpp>
#include <thi-sensor-fusion/realtime-stitcher/composer/composer.private.cuh>

#include <vector>
#include <limits>
#include <stdexcept>
#include <future>
#include <algorithm>
#include <fstream>
#include <filesystem>
#include <regex>
#include <atomic>

#include <iostream>

#include <Eigen/Dense>
#include <opencv2/stitching.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/core/eigen.hpp>


namespace thi_sensor_fusion {

class Progress {
public:
  Progress(const uint16_t max) : max(max) {
    current = 0;
  };

  inline Progress& operator++() {
    ++current;
    return *this;
  };

  const uint16_t max;
  std::atomic<uint16_t> current;
};

static std::mutex print_mutex;
static std::string last_message;

static void printMessage(const std::string &message);
static void printMessage(const std::string &message, const Progress &progress);

static inline cv::Ptr<cv::Stitcher> buildStitcher(const RealtimeStitcher::Settings &settings, uint8_t index = 0);

static inline cv::Mat computePanorama(cv::Ptr<cv::Stitcher> stitcher, const std::vector<cv::Mat> &frames, const std::vector<cv::detail::CameraParams> &parameters);

static inline cv::Mat generateXPattern(uint16_t x, uint16_t y);
static inline cv::Mat generateYPattern(uint16_t x, uint16_t y);

static inline std::vector<cv::Mat> generateWeightMaps(std::vector<cv::Mat> &patterns, const RealtimeStitcher::Settings &settings);

static inline std::vector<bool> selectIndecesUpperCenter(const std::vector<bool> &indeces);
static inline std::vector<bool> selectIndecesLowerCenter(const std::vector<bool> &indeces);

static inline void warpPanorama(cv::OutputArray output, cv::cuda::GpuMat &input, float heading, float pitch, float zoom, const RealtimeStitcher::Settings &settings, cv::cuda::Stream &stream);

static inline double getDimensionFactor(const RealtimeStitcher::Settings &settings);

static inline std::vector<cv::detail::CameraParams> convertParameters(const std::vector<RealtimeStitcher::CameraParameters> &parameters, const RealtimeStitcher::Settings &settings, bool compensate_registration_scaling = true);
static inline std::vector<RealtimeStitcher::CameraParameters> convertParameters(const std::vector<cv::detail::CameraParams> &internal_parameters, const RealtimeStitcher::Settings &settings, bool compensate_registration_scaling = true);

static inline cv::Vec3b IntToVec(uint16_t input);
static inline uint16_t VecToInt(const cv::Vec3b &input);


RealtimeStitcher::RealtimeStitcher(const Settings &settings, const std::vector<CameraParameters> &parameters) : lookup_buffer(*new LookupBuffer()) {
  setSettings(settings);
  setCameraParameters(parameters);

  // Calculate the lookup data for this run only.
  calculateTransformations();
}

RealtimeStitcher::RealtimeStitcher(const Settings &settings, const std::vector<CameraParameters> &parameters, const std::string &path) : lookup_buffer(*new LookupBuffer()) {
  setSettings(settings);
  setCameraParameters(parameters);

  if (std::filesystem::exists(path)) {
    // Load the cached lookup generated if the file exists.
    loadLookupData(path);
  } else {
    // Calculate the lookup data and store it afterwards for further runs.
    calculateTransformations();
    storeLookupData(path);
  }
}

std::vector<RealtimeStitcher::CameraParameters> RealtimeStitcher::estimateCameraParameters(const std::vector<cv::Mat> &frames) const {
  cv::Ptr<cv::Stitcher> stitcher = buildStitcher(settings);

  // Let the OpenCV stitcher estimate the parameters.
  stitcher->estimateTransform(frames);

  // Convert to our own format.
  return convertParameters(stitcher->cameras(), settings, false);
}

void RealtimeStitcher::calculateTransformations() {
  printMessage("Started preprocessing");

  // More than 255 cameras are currently not supported.
  if (parameters.size() > std::numeric_limits<uint8_t>::max()) {
    throw new std::runtime_error("Too many cameras.");
  }

  // Convert the parameters and compute the panoramas.
  std::vector<cv::detail::CameraParams> internal_parameters = convertParameters(parameters, settings);
  const std::vector<thi_sensor_fusion::RealtimeStitcher::CameraParameters> params = convertParameters(internal_parameters, settings);

  std::counting_semaphore available_threads(settings.threads_cpu);
  Progress position_progress(2 * parameters.size());

  printMessage("Generating position lookup maps", position_progress);

  // Lamda to generate a lookup panorama for one dimension.
  auto generatePanorama = [this, &internal_parameters, &available_threads, &position_progress](cv::Mat (*generator)(uint16_t, uint16_t), uint8_t index) -> cv::Mat {
    // Decrement the semaphore to not exceed the maximum thread count.
    available_threads.acquire();

    cv::Ptr<cv::Stitcher> stitcher = buildStitcher(settings, index);

    // Fake input cameras.
    std::vector<cv::Mat> patterns(parameters.size());

    // Generate preprocessing pattern.
    for (uint8_t i = 0; const CameraParameters &parameter : parameters) {
      cv::Mat pattern;

      if (i == index) {
        // Only use the supplied generator on the wanted camera index.
        pattern = generator(parameter.dimensions.x, parameter.dimensions.y);
      } else {
        // Use empty frames for all other cameras in order to not affect the output.
        pattern = cv::Mat::zeros(parameter.dimensions.y, parameter.dimensions.x, CV_8UC3);
      }

      // Resize the input frame to generate panoramas with higher resolutions.
      const cv::Size registration_size(pattern.cols * settings.registration_scaling.x, pattern.rows * settings.registration_scaling.y);
      cv::resize(pattern, patterns[i], registration_size, settings.registration_scaling.x, settings.registration_scaling.y, cv::INTER_NEAREST);

      ++i;
    }

    // Compute the panorama using the OpenCV stitcher.
    const cv::Mat output = computePanorama(stitcher, patterns, internal_parameters);

    // Increment the semaphore again.
    available_threads.release();

    printMessage("Generating position lookup maps", ++position_progress);

    return output;
  };

  const uint8_t sources = parameters.size();

  std::vector<std::future<cv::Mat>> x_panorama_futures(sources);
  std::vector<std::future<cv::Mat>> y_panorama_futures(sources);

  // Create a future for each panorama.  
  for (uint8_t i = 0; i < sources; ++i) {
    x_panorama_futures[i] = std::async(std::launch::async, generatePanorama, generateXPattern, i);
    y_panorama_futures[i] = std::async(std::launch::async, generatePanorama, generateYPattern, i);
  }

  // There will be one panorama for every input frame and dimension.
  std::vector<cv::Mat> x_panoramas(sources);
  std::vector<cv::Mat> y_panoramas(sources);

  // Fill in the panoramas from the furures.
  for (uint8_t i = 0; i < sources; ++i) {
    x_panoramas[i] = x_panorama_futures[i].get();
    y_panoramas[i] = y_panorama_futures[i].get();
  }

  // Every panorama has the same dimensions, so we can just copy one.
  const uint32_t rows = x_panoramas[0].rows;
  const uint32_t cols = x_panoramas[0].cols;

  printMessage("Lookup buffer will have the dimensions " + std::to_string(rows) + "x" + std::to_string(cols));

  // Generate a custom weight map from any dimension.
  const std::vector<cv::Mat> weight_maps = generateWeightMaps(x_panoramas, settings);

  // Allocate the lookup buffer.
  lookup_buffer.setDimensions(rows, cols, sources);

  // Get a template of the lookup buffer stored in CPU memory.
  cv::Mat lookup_buffer_local = lookup_buffer.getTemplate();

  /// Fill in the template.
  for (uint64_t x = 0; x < lookup_buffer.getDimensionX(); ++x) {
    for (uint64_t y = 0; y < lookup_buffer.getDimensionY(); ++y) {
      for (uint8_t i = 0; i < sources; ++i) {
        // Get a reference to the lookup element at this index.
        LookupElement &element = lookup_buffer_local.at<LookupElement>(x, (y * sources) + i);

        // Fill in the lookup data.
        element.weight = weight_maps[i].at<float>(x, y);
        element.position.x = VecToInt(x_panoramas[i].at<cv::Vec3b>(x, y));
        element.position.y = VecToInt(y_panoramas[i].at<cv::Vec3b>(x, y));
      }
    }
  }

  // Upload the lookup buffer to the GPU.
  lookup_buffer.setBuffer(lookup_buffer_local);

  printMessage("Preprocessing completed");
}

void RealtimeStitcher::storeLookupData(const std::string &path) const {
  std::ofstream file(path, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);

  // Write major version number.
  file.write(reinterpret_cast<const char *>(&version), sizeof(version));

  const uint16_t dimension_x = lookup_buffer.getDimensionX();
  const uint16_t dimension_y = lookup_buffer.getDimensionY();
  const uint8_t dimension_index = lookup_buffer.getDimensionIndex();

  // Write dimensions.
  file.write(reinterpret_cast<const char *>(&dimension_x), sizeof(dimension_x));
  file.write(reinterpret_cast<const char *>(&dimension_y), sizeof(dimension_y));
  file.write(reinterpret_cast<const char *>(&dimension_index), sizeof(dimension_index));

  // Download the buffer to CPU memory.
  cv::cuda::GpuMat gpu_buffer = lookup_buffer.getMat();
  cv::Mat lookup_buffer_local;
  gpu_buffer.download(lookup_buffer_local);

  // Write the buffer.
  for (uint64_t x = 0; x < dimension_x; ++x) {
    for (uint64_t y = 0; y < dimension_y; ++y) {
      for (uint8_t i = 0; i < dimension_index; ++i) {
        // Get a reference to the lookup element at this index.
        const LookupElement &element = lookup_buffer_local.at<LookupElement>(x, (y * dimension_index) + i);

        // Just write the raw memory into the file.
        file.write(reinterpret_cast<const char *>(&element), sizeof(element));
      }
    }
  }
}

void RealtimeStitcher::loadLookupData(const std::string &path) {
  std::ifstream file(path, std::ios_base::in | std::ios_base::binary);

  // Read major version number.
  uint16_t file_version;
  file.read(reinterpret_cast<char *>(&file_version), sizeof(file_version));

  if (file_version != version) {
    throw new std::runtime_error("Lookup data file version mismatch.");
  }

  uint16_t dimension_x;
  uint16_t dimension_y;
  uint8_t dimension_index;

  // Read dimensions.
  file.read(reinterpret_cast<char *>(&dimension_x), sizeof(dimension_x));
  file.read(reinterpret_cast<char *>(&dimension_y), sizeof(dimension_y));
  file.read(reinterpret_cast<char *>(&dimension_index), sizeof(dimension_index));

  // Allocate the lookup buffer.
  lookup_buffer.setDimensions(dimension_x, dimension_y, dimension_index);

  // Get a template of the lookup buffer stored in CPU memory.
  cv::Mat lookup_buffer_local = lookup_buffer.getTemplate();

  // Read the lookup buffer.
  for (uint64_t x = 0; x < dimension_x; ++x) {
    for (uint64_t y = 0; y < dimension_y; ++y) {
      for (uint8_t i = 0; i < dimension_index; ++i) {
        // Get a reference to the lookup element at this index.
        LookupElement &element = lookup_buffer_local.at<LookupElement>(x, (y * dimension_index) + i);

        // Just read the raw data from the file.
        file.read(reinterpret_cast<char *>(&element), sizeof(element));
      }
    }
  }

  // Upload the lookup buffer to the GPU.
  lookup_buffer.setBuffer(lookup_buffer_local);
}

void RealtimeStitcher::compose(cv::OutputArray output, cv::InputArrayOfArrays frames, float heading, float pitch, float zoom, cv::cuda::Stream &stream) const {
  std::vector<cv::cuda::GpuMat> gpu_frames;
  frames.getGpuMatVector(gpu_frames);
  
  // We require the exact amount of frames used in preprocessing.
  if (gpu_frames.size() != parameters.size()) {
    throw new std::runtime_error("The number of frames must match the number of camera parameters registered.");
  }

  // Run the raw frame generation on the GPU.
  cv::cuda::GpuMat lookup_output;
  callComposeKernel(lookup_buffer, gpu_frames.data(), lookup_output, settings.threads_gpu.x, settings.threads_gpu.y, stream);

  // Warp the raw frame afterwards.
  warpPanorama(output, lookup_output, heading, pitch, zoom, settings, stream);
}

static void printMessage(const std::string &message) {
  const std::lock_guard guard(print_mutex);

  std::cout << std::endl << message << "." << std::flush;

  last_message = message;
}

static void printMessage(const std::string &message, const Progress &progress) {
  const std::lock_guard guard(print_mutex);

  if (last_message == message) {
    std::cout << "\r";
  } else {
    std::cout << std::endl;
  }

  const float percentage = (float)progress.current / (float)progress.max * 100;
  std::cout << "[" << std::fixed << std::setw(3) << std::setprecision(0) << percentage << "%] " << message << "..." << std::flush;

  last_message = message;
}

static inline cv::Ptr<cv::Stitcher> buildStitcher(const RealtimeStitcher::Settings &settings, uint8_t index) {
  cv::Ptr<cv::Stitcher> stitcher = cv::Stitcher::create();

  // Copy settings.
  stitcher->setCompositingResol(settings.compositing_resolution);
  stitcher->setRegistrationResol(settings.registration_resolution);
  cv::ocl::setUseOpenCL(settings.enable_opencl);

  // Disable unused features.
  stitcher->setExposureCompensator(cv::makePtr<cv::detail::NoExposureCompensator>());
  stitcher->setSeamFinder(cv::makePtr<cv::detail::NoSeamFinder>());

  // Set static settings.
  stitcher->setInterpolationFlags(cv::INTER_NEAREST);
  stitcher->setWarper(cv::makePtr<cv::StereographicWarper>());
  stitcher->setWaveCorrection(true);

  // Set custom blender.
  stitcher->setBlender(cv::makePtr<SelectiveBlender>(index));

  return stitcher;
}

static inline cv::Mat computePanorama(cv::Ptr<cv::Stitcher> stitcher, const std::vector<cv::Mat> &frames, const std::vector<cv::detail::CameraParams> &parameters) {
  cv::Mat panorama;
  
  // Register the camera positions with the OpenCV stitcher and let it compose the panorama.
  stitcher->setTransform(frames, parameters);
  stitcher->composePanorama(frames, panorama);

  return panorama;
}

static inline cv::Mat generateXPattern(uint16_t x, uint16_t y) {
  cv::Mat pattern = cv::Mat(y, x, CV_8UC3);

  // Encode all x-positions.
  for (uint16_t y = 0; y < pattern.cols; ++y) {
    for (uint16_t x = 0; x < pattern.rows; ++x) {
      pattern.at<cv::Vec3b>(x, y) = IntToVec(x);
    }
  }

  return pattern;
}

static inline cv::Mat generateYPattern(uint16_t x, uint16_t y) {
  cv::Mat pattern = cv::Mat(y, x, CV_8UC3);

  // Encode all y-positions.
  for (uint16_t y = 0; y < pattern.cols; ++y) {
    for (uint16_t x = 0; x < pattern.rows; ++x) {
      pattern.at<cv::Vec3b>(x, y) = IntToVec(y);
    }
  }

  return pattern;
}

static inline std::vector<cv::Mat> generateWeightMaps(std::vector<cv::Mat> &patterns, const RealtimeStitcher::Settings &settings) {
  // Shared size for all buffers.
  const cv::Size size = patterns[0].size();

  Progress weight_progress(size.width + 1);

  // Buffers for every input camera with its local summed weight.
  std::vector<cv::Mat> sum_buffers(patterns.size());

  // Initialize the sum buffers with zeros.
  for (cv::Mat &map : sum_buffers) {
    map = cv::Mat::zeros(size, CV_32S);
  }

  // Function pointer to the indeces selector function (priority method).
  std::vector<bool> (*select_indeces)(const std::vector<bool> &);

  // Set the indeces selector based on the settings.
  switch (settings.blending_priority_method) {
    case (RealtimeStitcher::Settings::PriorityMethod::upper_center): {
      select_indeces = selectIndecesUpperCenter;
      break;
    }

    case (RealtimeStitcher::Settings::PriorityMethod::lower_center): {
      select_indeces = selectIndecesLowerCenter;
      break;
    }
  }

  // Calculate absolute blending range in pixels.
  const double blending_range = settings.blending_sharpness * std::sqrt(std::pow(size.width, 2) + std::pow(size.height, 2));
  const int16_t blending_range_x = blending_range * settings.registration_scaling.x;
  const int16_t blending_range_y = blending_range * settings.registration_scaling.y;


  // Iterate through every sum buffer pixel.
  for (int16_t y = 0; y < size.width; ++y) {
    for (int16_t x = 0; x < size.height; ++x) {
      std::vector<bool> local_sources(patterns.size());
      uint8_t number_local_sources = 0;

      // Store the number and indeces of input frames present at the current pixel.
      for (uint8_t source = 0; source < patterns.size(); ++source) {
        const uint8_t value = patterns[source].at<cv::Vec3b>(x, y)[2];

        if (value > 0) {
          local_sources[source] = true;
          ++number_local_sources;
        }
      }

      // Make a selection of wanted sources at this pixel.
      const std::vector<bool> selected_indeces = select_indeces(local_sources);

      // Apply the effect due to the selcetion on the weight for every source.  
      for (uint8_t i = 0; i < patterns.size(); ++i) {
        if (!selected_indeces[i]) {
          // Do not affect the weights if the source has not been selected.
          continue;
        }

        // Affect pixels in the blending range if the source has been selected.
        for (int16_t dy = std::max(0, y - blending_range_y); dy <= std::min(size.width - 1, y + blending_range_y); ++dy) {
          for (int16_t dx = std::max(0, x - blending_range_x); dx <= std::min(size.height - 1, x + blending_range_x); ++dx) {
            if (patterns[i].at<cv::Vec3b>(dx, dy)[2] > 0) {
              // If the source is present at the offset pixel, increase its weight there.
              ++sum_buffers[i].at<int32_t>(dx, dy);
            } else if (number_local_sources > 1) {
              // If the source is not present at the offset pixel and other sources are present there, decrease its weight at the current pixel.
              --sum_buffers[i].at<int32_t>(x, y);
            }
          }
        }
      }
    }

    printMessage("Generating weight lookup maps", ++weight_progress);
  }

  // Output weight maps.
  std::vector<cv::Mat> output(patterns.size());

  // Allocate the output maps.
  for (cv::Mat &map : output) {
    map = cv::Mat(size, CV_32F);
  }

  // Iterate through every output pixel.
  for (int16_t y = 0; y < size.width; ++y) {
    for (int16_t x = 0; x < size.height; ++x) {
      // Sum of sums generated at the local pixel.
      int32_t local_sum = 0;

      // Itherate through all sources.
      for (uint8_t i = 0; i < patterns.size(); ++i) {
        // Saturate the value.
        const int32_t value = std::max(0, sum_buffers[i].at<int32_t>(x, y));
        sum_buffers[i].at<int32_t>(x, y) = value;

        // Add to the local sum.
        local_sum += value;
      }

      if (local_sum == 0) {
        for (uint8_t i = 0; i < patterns.size(); ++i) {
          // Set all weights at this pixel to zero if there are no weights here.
          output[i].at<float>(x, y) = 0;
        }
      } else {
        for (uint8_t i = 0; i < patterns.size(); ++i) {
          // Calculate the portion of every source at this pixel.
          output[i].at<float>(x, y) = (float)sum_buffers[i].at<int32_t>(x, y) / (float)local_sum;
        }
      }
    }
  }

  printMessage("Generating weight lookup maps", ++weight_progress);

  return output;
}

static inline std::vector<bool> selectIndecesUpperCenter(const std::vector<bool> &indeces) {
  std::vector<bool> output(indeces.size());

  // Modulo index access lamda.
  auto get_value = [&indeces](uint8_t offset, uint8_t index = 0) -> bool {
    return indeces[(offset + index) % indeces.size()];
  };

  for (uint8_t i = 0; i < indeces.size(); ++i) {
    // Boolean logic for index selection.
    const bool logic = get_value(i) && (!(get_value(i, 1) || get_value(i, 2)) || (get_value(i, 2) && get_value(i, 3)));

    output[i] = logic;
  }

  return output;
}

static inline std::vector<bool> selectIndecesLowerCenter(const std::vector<bool> &indeces) {
  std::vector<bool> output(indeces.size());

  // Modulo index access lamda.
  auto get_value = [&indeces](uint8_t offset, uint8_t index = 0) -> bool {
    return indeces[(offset + index) % indeces.size()];
  };

  for (uint8_t i = 0; i < indeces.size(); ++i) {
    // Boolean logic for index selection.
    const bool logic = get_value(i) && (!(get_value(i, 3) || get_value(i, 2)) || (get_value(i, 2) && get_value(i, 1)));

    output[i] = logic;
  }

  return output;
}

static inline void warpPanorama(cv::OutputArray output, cv::cuda::GpuMat &input, float heading, float pitch, float zoom, const RealtimeStitcher::Settings &settings, cv::cuda::Stream &stream) {
  // Translate the image, so the center of the image is at the origin.
  Eigen::Matrix3f transform_pretranslate = Eigen::Matrix3f::Identity();
  transform_pretranslate.block(0, 2, 2, 1) = Eigen::Vector2f(-input.rows / 2, -input.cols / 2);

  // Rotate the image to adjust heading.
  Eigen::Matrix3f transform_heading = Eigen::Matrix3f::Identity();
  transform_heading.block(0, 0, 2, 2) = Eigen::Rotation2D(heading).toRotationMatrix();

  // Translate the image to adjust pitch.
  Eigen::Matrix3f transform_pitch = Eigen::Matrix3f::Identity();
  transform_heading.block(0, 2, 2, 1) = Eigen::Vector2f(0, input.cols * pitch / settings.visible_pitch_angle);

  // Scale the image to adjust zoom.
  Eigen::Matrix3f transform_zoom = Eigen::Matrix3f::Identity();
  transform_zoom.block(0, 0, 2, 2) = zoom * Eigen::Matrix2f::Identity();

  // Translate the image, so the top left corner of the image is at the origin.
  Eigen::Matrix3f transform_posttranslate = Eigen::Matrix3f::Identity();
  transform_posttranslate.block(0, 2, 2, 1) = Eigen::Vector2f(input.rows / 2, input.cols / 2);

  // Scale the image to the desired resolution.
  Eigen::Matrix3f transform_postscale = Eigen::Matrix3f::Identity();
  const float scale_factor = std::min((float)settings.output_resolution.x / (float)input.rows, (float)settings.output_resolution.y / (float)input.cols);
  transform_postscale.block(0, 0, 2, 2) = scale_factor * Eigen::Matrix2f::Identity();

  // Combine the transformations.
  const Eigen::Matrix3f transform = transform_postscale * transform_posttranslate * transform_zoom * transform_pitch * transform_heading * transform_pretranslate;
  Eigen::Matrix<float, 2, 3> partial_transform = transform.block(0, 0, 2, 3);

  // Convert the transformation matrix to OpenCV.
  cv::Mat transform_data;
  cv::eigen2cv(partial_transform, transform_data);

  // Use the GPU to apply the transformation.
  cv::Size output_size(settings.output_resolution.x, settings.output_resolution.y);
  cv::cuda::warpAffine(input, output, transform_data, output_size, cv::INTER_CUBIC, cv::BORDER_CONSTANT, cv::Scalar(), stream);
}

static inline double getDimensionFactor(const RealtimeStitcher::Settings &settings) {
  // Calculate the required conversion factor for internal to OpenCV scaling.
  return std::max(1.0, std::sqrt(settings.compositing_resolution / settings.registration_resolution));
}

static inline std::vector<cv::detail::CameraParams> convertParameters(const std::vector<RealtimeStitcher::CameraParameters> &parameters, const RealtimeStitcher::Settings &settings, bool compensate_registration_scaling) {
  std::vector<cv::detail::CameraParams> internal_parameters(parameters.size());

  const double registration_scaling_x = compensate_registration_scaling ? settings.registration_scaling.x : 1;
  const double registration_scaling_y = compensate_registration_scaling ? settings.registration_scaling.y : 1;

  for (uint8_t i = 0; cv::detail::CameraParams &internal_parameter : internal_parameters) {
    const RealtimeStitcher::CameraParameters &parameter = parameters[i];

    // Convert the camera parameters.
    internal_parameter.focal = parameter.focal * std::sqrt(registration_scaling_x * registration_scaling_y);
    internal_parameter.aspect = parameter.aspect;

    // Convert the resolutions.
    const double dimension_factor = getDimensionFactor(settings);
    internal_parameter.ppx = (double)parameter.dimensions.x / 2 / dimension_factor * registration_scaling_x;
    internal_parameter.ppy = (double)parameter.dimensions.y / 2 / dimension_factor * registration_scaling_y;

    // Convert the rotations to radians.
    const float heading = parameter.rotation.heading / 180 * std::numbers::pi;
    const float pitch = parameter.rotation.pitch / 180 * std::numbers::pi;
    const float roll = parameter.rotation.roll / 180 * std::numbers::pi;

    // Create a rotation matrix.
    Eigen::Matrix3f rotation = (Eigen::AngleAxisf(heading, Eigen::Vector3f::UnitY()) * Eigen::AngleAxisf(pitch, Eigen::Vector3f::UnitX()) * Eigen::AngleAxisf(roll, Eigen::Vector3f::UnitZ())).toRotationMatrix();
    cv::eigen2cv(rotation, internal_parameter.R);

    // Set the translation to zero as it is not yet supported by OpenCV.
    internal_parameter.t = cv::Mat(cv::Size2i(1, 3), CV_64F);
    internal_parameter.t.at<double>(0, 0) = 0;
    internal_parameter.t.at<double>(0, 1) = 0;
    internal_parameter.t.at<double>(0, 2) = 0;

    ++i;
  }

  return internal_parameters;
}

static inline std::vector<RealtimeStitcher::CameraParameters> convertParameters(const std::vector<cv::detail::CameraParams> &internal_parameters, const RealtimeStitcher::Settings &settings, bool compensate_registration_scaling) {
  std::vector<RealtimeStitcher::CameraParameters> parameters(internal_parameters.size());

  const double registration_scaling_x = compensate_registration_scaling ? settings.registration_scaling.x : 1;
  const double registration_scaling_y = compensate_registration_scaling ? settings.registration_scaling.y : 1;

  for (uint8_t i = 0; RealtimeStitcher::CameraParameters &parameter : parameters) {
    const cv::detail::CameraParams &internal_parameter = internal_parameters[i];

    // Convert the camera parameters.
    parameter.focal = internal_parameter.focal / std::sqrt(registration_scaling_x * registration_scaling_y);
    parameter.aspect = internal_parameter.aspect;

    // Convert the resolutions.
    const double dimension_factor = getDimensionFactor(settings);
    parameter.dimensions.x = (uint16_t)(internal_parameter.ppx * 2 * dimension_factor / registration_scaling_x);
    parameter.dimensions.y = (uint16_t)(internal_parameter.ppy * 2 * dimension_factor / registration_scaling_y);

    // Calculate the euler angles from the rotation matrix.
    Eigen::Matrix3f rotation;
    cv::cv2eigen(internal_parameter.R, rotation);
    Eigen::Vector3f euler = rotation.eulerAngles(1, 0, 2);

    const float heading = euler[0];
    const float pitch = euler[1];
    const float roll = euler[2];

    // Convert the rotations to degrees.
    parameter.rotation.heading = heading * 180 / std::numbers::pi;
    parameter.rotation.pitch = pitch * 180 / std::numbers::pi;
    parameter.rotation.roll = roll * 180 / std::numbers::pi;

    ++i;
  }

  return parameters;
}

static inline cv::Vec3b IntToVec(uint16_t input) {
  cv::Vec3b output;

  // Encode the input.
  output[0] = (input & 0xff) >> 0;
  output[1] = (input & 0xff00) >> 8;
  
  // Fill in the remaining byte to get free blending information.
  output[2] = std::numeric_limits<uint8_t>::max();

  return output;
}

static inline uint16_t VecToInt(const cv::Vec3b &input) {
  // Decode the input.
  uint16_t output = input[0] << 0;
  output |= input[1] << 8;

  return output;
}

}
