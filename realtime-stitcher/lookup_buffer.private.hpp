#pragma once

#include <cstdint>

#include <opencv2/core.hpp>
#include <opencv2/core/traits.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/cudev/ptr2d/gpumat.hpp>

namespace thi_sensor_fusion {

struct LookupElement;
class LookupBuffer;

}

namespace cv {

template<> class DataType<thi_sensor_fusion::LookupElement> {
public:
  typedef thi_sensor_fusion::LookupElement value_type;
  typedef double work_type;
  typedef value_type channel_type;
  typedef value_type vec_type;

  enum {
    generic_type = 0,
    depth = CV_64F,
    channels = 1,
    fmt = (int)'u',
    type = CV_MAKETYPE(depth, channels)
  };
};

}

namespace thi_sensor_fusion {

struct LookupElement {
  float weight;
  
  struct Position {
    uint16_t x;
    uint16_t y;
  } position;
};

class LookupBuffer {
public:
inline void setDimensions(uint16_t x, uint16_t y, uint8_t index) {
  dimensions.x = x;
  dimensions.y = y;
  dimensions.index = index;
}

inline void setBuffer(cv::Mat &input) {
  buffer.upload(input);
}

inline uint64_t getDimension() const {
  return ((uint64_t)getDimensionX()) * ((uint64_t)getDimensionY()) * ((uint64_t)getDimensionIndex());
}

inline uint16_t getDimensionX() const {
  return dimensions.x;
}

inline uint16_t getDimensionY() const {
  return dimensions.y;
}

inline uint8_t getDimensionIndex() const {
  return dimensions.index;
}

inline cv::Mat getTemplate() const {
  return cv::Mat_<LookupElement>(getDimensionX(), getDimensionY() * getDimensionIndex());
}

inline const cv::cuda::GpuMat &getMat() const {
  return buffer;
}

private:
  struct Dimensions {
    uint16_t x;
    uint16_t y;
    uint8_t index;
  } dimensions;

  cv::cudev::GpuMat_<LookupElement> buffer;
};

}
